﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject WallPrefab;
    public GameObject CubeObstaclePrefab;
    public GameObject CollectablePrefab;
    public TextMeshProUGUI ScoreText;

    //movement
    private float horizontalSpeed = 10;
    private float verticalSpeed = 3;
    private float positionEdge = 4.5f;

    private bool isMoving = false;


    //direction tracking
    private bool isMovingLeft = false;
    private bool isMovingRight = false;
    private bool isMovingUp = false;
    private bool isMovingDown = false;

    //obstacles
    private IList<GameObject> obstacles = new List<GameObject>();
    private float spawnInterval = 1.1f;
    private bool spawnLeft = false;

    private int totalObstacleCount = 0;
    private int stageProgressCount = 0;
    private int stageProgressInterval = 6;

    public int score = 0;

    void Start()
    {
        SetupScene();
    }

    void Update()
    {
        var playerRb = Player.GetComponent<Rigidbody>();

        SetScore(++score);

        HandleInput(playerRb);
        DoMovement(playerRb);
    }

    private void HandleInput(Rigidbody targetRigidbody)
    {
        if (Input.GetButton("Horizontal"))
        {
            HorizontalInput(targetRigidbody);
        }
        if (Input.GetButton("Vertical"))
        {
            VerticalInput(targetRigidbody);
        }
    }

    private void HorizontalInput(Rigidbody targetRigidbody)
    {
        var horizontal = Input.GetAxis("Horizontal");
        var direction = 0;
        //user input move left
        if (horizontal < 0)
        {
            direction = -1;
            //at or beyond left edge
            if (targetRigidbody.transform.position.x <= -1 * positionEdge)
            {
                targetRigidbody.transform.position.Set((float)positionEdge * direction + .5f, targetRigidbody.transform.position.y, targetRigidbody.transform.position.z);
                isMovingLeft = false;
                Debug.Log("hit left wall");
            }
            else
            {
                isMovingLeft = true;
            }
        }
        //user input move right
        else if (horizontal > 0)
        {
            direction = 1;
            //at or beyond right edge
            if (targetRigidbody.transform.position.x >= positionEdge)
            {
                targetRigidbody.transform.position.Set((float)positionEdge * direction - .5f, targetRigidbody.transform.position.y, targetRigidbody.transform.position.z);
                isMovingRight = false;
                Debug.Log("hit right wall");
            }
            else
            {
                isMovingRight = true;
            }
        }
    }

    private void VerticalInput(Rigidbody targetRigidbody)
    {
        //todo: negative is up, positive it down
        var vertical = Input.GetAxis("Vertical");
        var direction = 0;

        if(vertical < 0)
        {
            //go up
        }
        else if(vertical > 0)
        {
            //go down
        }


        if (vertical < 0)
        {
            //direction = -1;
            ////at or beyond top
            //if (targetRigidbody.transform.position.y <= -1 * positionEdge) //position edge is not how this should be defined. what to use instead?
            //{
            //    targetRigidbody.transform.position.Set((float)positionEdge * direction + .5f, targetRigidbody.transform.position.y, targetRigidbody.transform.position.z);
            //    isAtLeftEdge = true;
            //    isMovingLeft = false;
            //    Debug.Log("hit left wall");
            //}
            //else
            //{
            //    isMovingLeft = true;
            //}
            targetRigidbody.MovePosition(new Vector3(targetRigidbody.position.x, (targetRigidbody.position.y - (Time.deltaTime * verticalSpeed)), targetRigidbody.position.z));
        }
        else if (vertical > 0)
        {
            targetRigidbody.MovePosition(new Vector3(targetRigidbody.position.x, (targetRigidbody.position.y + (Time.deltaTime * verticalSpeed)), targetRigidbody.position.z));
        }
    }

    private void DoMovement(Rigidbody playerRigidbody)
    {
        //keeping this here to more easily switch back to magnet-snapping movement
        if (isMovingLeft)
        {
            playerRigidbody.MovePosition(new Vector3((playerRigidbody.position.x - (Time.deltaTime * horizontalSpeed)), playerRigidbody.position.y, playerRigidbody.position.z));
            isMovingLeft = false;
        }
        else if (isMovingRight)
        {
            playerRigidbody.MovePosition(new Vector3((playerRigidbody.position.x + (Time.deltaTime * horizontalSpeed)), playerRigidbody.position.y, playerRigidbody.position.z));
            isMovingRight = false;
        }

        //todo: come back to this
        //if (isMovingDown)
        //{
        //    playerRigidbody.MovePosition(new Vector3((playerRigidbody.position.x + (Time.deltaTime * horizontalSpeed)), playerRigidbody.position.y, playerRigidbody.position.z));
        //    isMovingDown = false;
        //}
        //else if (isMovingUp)
        //{
        //    playerRigidbody.MovePosition(new Vector3((playerRigidbody.position.x + (Time.deltaTime * horizontalSpeed)), playerRigidbody.position.y, playerRigidbody.position.z));
        //    isMovingUp = false;
        //}
    }

    private void SetScore(int newScore)
    {
        score = newScore;
        ScoreText.text = score.ToString();
    }

    //maybe add param to this later
    public void CollectObject()
    {
        var playerRigidbody = Player.GetComponent<Rigidbody>();
        SetScore(score + 500);
        playerRigidbody.velocity = new Vector3(0, 0, 0);
    }

    private void SetupScene()
    {
        Instantiate(WallPrefab, new Vector3(-6, 0, 0), Quaternion.identity);
        Instantiate(WallPrefab, new Vector3(6, 0, 0), Quaternion.identity);

        SpawnObstacles();
    }


    private void SpawnObstacles()
    {
        SpawnByCount();
    }

    private void SpawnOnSide()
    {
        Debug.Log($"{spawnLeft}");
        var obstacle = SpawnRandomObstacle();
        var adjustment = 1;
        if (spawnLeft == false)
        {
            adjustment = -1;
        }

        var xPos = Mathf.Abs(obstacle.transform.position.x) * adjustment;
        obstacle.transform.position = new Vector3(xPos, obstacle.transform.position.y, obstacle.transform.position.z);
        spawnLeft = !spawnLeft;
    }

    private void SpawnByCount()
    {
        InvokeRepeating(nameof(Spawn), 1f, spawnInterval);
    }

    private void Spawn()
    {
        if (stageProgressCount <= stageProgressInterval)
        {
            SpawnRandomObstacle();
        }
        else if(stageProgressCount <= stageProgressInterval * 2)
        {
            SpawnOnSide();
        }
        else
        {
            stageProgressCount = 0;
        }

        stageProgressCount++;
    }


    private GameObject SpawnRandomObstacle()
    {
        var randEdge = UnityEngine.Random.Range(0, 2);
        float scaleX = UnityEngine.Random.Range(1f, 8.75f);
        float randPosY = UnityEngine.Random.Range(0f, 1f);


        float randPosX;
        if (randEdge == 0) { randPosX = (-1f * positionEdge) + (scaleX/2f) - 0.5f; } else { randPosX = positionEdge - (scaleX/2f) + 0.5f; }
        var pos = new Vector3(randPosX, -6 + randPosY, 0);


        var scale = new Vector3(scaleX, 1, 3);

        var randSpawnChance = UnityEngine.Random.Range(0, 10);

        var instance = SpawnObstacle(CubeObstaclePrefab, pos, scale, randSpawnChance == 0);
        obstacles.Add(instance);
        return instance;
    }

    private void IncrementObstacleCount()
    {
        totalObstacleCount += 1;
    }

    private GameObject SpawnObstacle(GameObject obstacle, Vector3 position, Vector3 scale, bool withCollectable)
    {
        var velocity = new Vector3(0f, 3f);
        var instance = Instantiate(obstacle, position, Quaternion.identity);
        instance.transform.localScale = scale;
        instance.GetComponent<Rigidbody>().velocity = velocity;

        if (withCollectable)
        {
            //add collectable
            var collectablePosition = new Vector3(position.x, position.y + 1, position.z);
            var collectable = Instantiate(CollectablePrefab, collectablePosition, Quaternion.identity);
            collectable.GetComponent<Rigidbody>().velocity = velocity;
        }

        return instance;
    }
}
