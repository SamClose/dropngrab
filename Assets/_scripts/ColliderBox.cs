﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderBox : MonoBehaviour
{
    public GameObject NavigationManagerHolder;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag.Contains("Player"))
        {
            Debug.Log("game over");
            NavigationManager.LastScore = FindObjectOfType<GameManager>().score;
            NavigationManagerHolder.GetComponent<NavigationManager>().GoToScene("EndGameScene");
        }

        Destroy(collision.collider.gameObject);
    }
}
