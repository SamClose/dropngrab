﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGameUIScript : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;

    // Start is called before the first frame update
    void Start()
    {
        ScoreText.text = $"Score: {NavigationManager.LastScore}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
