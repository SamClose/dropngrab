﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        //if(collision.collider.tag == "Collectable")
        //{
        //    Destroy(collision.collider.gameObject);
        //    FindObjectOfType<GameManager>().CollectObject();
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Collectable")
        {
            Destroy(other.gameObject);
            FindObjectOfType<GameManager>().CollectObject();
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
